const express = require("express");
const app = express();

const cors = require("cors");

const port = process.env.PORT || 3000;

const helmet = require("helmet");
app.use(cors);
app.use(helmet());

app.get("/", (req, res, next) => {
  res.status(200).send("<h1>App is running!</h1>");
});

app.listen(port, () => console.log(`Server running on localhost:${port}`));
