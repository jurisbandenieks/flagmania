import React from "react"
import { BrowserRouter as Router, Route, Switch } from "react-router-dom"
import "bootstrap/dist/css/bootstrap.min.css"
import "./App.scss"
import Footer from "./components/layout/footer/Footer"
import Start from "./components/pages/Start/Start"
import Game from "./components/pages/game/Game"

import UserState from "./context/user/UserState"

function App() {
  return (
    <UserState>
      <Router>
        <div className="App text-center">
          <Switch>
            <Route exact path="/" component={Start}></Route>
            <Route exact path="/game" component={Game}></Route>
          </Switch>
          <Footer />
        </div>
      </Router>
    </UserState>
  )
}

export default App
