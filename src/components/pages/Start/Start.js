import React, { useContext } from "react"
import { Link } from "react-router-dom"
import "./Start.scss"

import UserContext from "../../../context/user/userContext"

const Start = () => {
  const userContext = useContext(UserContext)

  const chooseLevel = lvl => {
    userContext.setLevel(lvl)
  }
  return (
    <div className="start">
      <Link to="/game" className="start__link">
        <div
          className="start__selection start__selection--easy"
          onClick={param => chooseLevel({ text: "easy", value: 0 })}
        >
          <h1 className="start__text start__text--easy">Easy</h1>
        </div>
      </Link>
      <Link to="/game" className="start__link">
        <div
          className="start__selection start__selection--medium"
          onClick={param => chooseLevel({ text: "medium", value: 1 })}
        >
          <h1 className="start__text start__text--medium">Medium</h1>
        </div>
      </Link>
      <Link to="/game" className="start__link">
        <div
          className="start__selection start__selection--hard"
          onClick={param => chooseLevel({ text: "hard", value: 2 })}
        >
          <h1 className="start__text start__text--hard">Hard</h1>
        </div>
      </Link>
    </div>
  )
}

export default Start
