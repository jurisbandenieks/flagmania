import React, { useContext, useEffect, useState } from "react"
import { Link } from "react-router-dom"

import Button from "react-bootstrap/Button"
import ProgressBar from "react-bootstrap/ProgressBar"

import Dialog from "../../layout/dialog/Dialog"
import Flags from "./flags/Flags"

import UserContext from "../../../context/user/userContext"

import "./Game.scss"

const Game = () => {
  const time = new Date()

  const userContext = useContext(UserContext)
  const { level, countries, getUsers } = userContext

  const [game, setGame] = useState({
    flags: [],
    correct: null,
    points: 0,
    streak: 1,
    timer: 60,
    open: false,
    show: false
  })
  const { flags, correct, points, streak, timer, open, show } = game

  useEffect(() => {
    nextFlags()
    getUsers()
    const interval = countdown()
    return () => clearInterval(interval)
    // eslint-disable-next-line
  }, [])

  const countdown = () => {
    setTimeout(
      setInterval(() => {
        const timeNow = new Date()
        const timePassed =
          timer - Math.abs(Math.round(((time - timeNow) * 2) / 1000))
        if (timePassed > 0) {
          setGame(game => ({ ...game, timer: timePassed }))
        } else {
          setGame(game => ({ ...game, timer: 0, show: true, open: false }))
        }
      }, 500),
      2000
    )
  }

  const nextFlags = () => {
    let i = 0
    let flagArr = []
    let randomNum = null

    while (i < 4) {
      randomNum = getRandomNum(0, countries[level.value].length - 1)
      let index = flagArr.indexOf(randomNum)
      if (index === -1) {
        flagArr.push(randomNum)
        i++
      }
    }
    const rightOne = flagArr[getRandomNum(0, 3)]
    setTimeout(
      () =>
        setGame(game => ({
          ...game,
          flags: flagArr,
          correct: rightOne,
          open: true
        })),
      500
    )
  }

  const guess = g => {
    if (g === countries[level.value][correct]) {
      setGame(game => ({
        ...game,
        open: false,
        points: points + 10 + (level.value + 1) * 3 * streak,
        streak: streak + 1
      }))
      nextFlags()
    } else {
      setGame(game => ({ ...game, streak: 1 }))
    }
  }

  const getRandomNum = (min, max) => {
    return Math.floor(Math.random() * (max - min + 1) + min)
  }

  return (
    <div className="game">
      <div className="game__headline">
        <h3>{countries[level.value][correct]}</h3>
      </div>
      <div className="container game__content">
        <ProgressBar variant="info" now={timer} max={60} srOnly />
        <div className="game__flags">
          <Flags flags={flags} guess={guess} open={open} />
        </div>
      </div>
      <Dialog show={show} points={points} />
      <div className="game__points">
        <h4>{points}</h4>
      </div>
    </div>
  )
}

export default Game
