import React, { useContext, Fragment } from "react"
import { CSSTransition, TransitionGroup } from "react-transition-group"
import UserContext from "../../../../context/user/userContext"

import "./Flags.css"

const Flags = props => {
  const userContext = useContext(UserContext)

  const { level, countries } = userContext
  const { open, guess, flags } = props

  return (
    <Fragment>
      <TransitionGroup className="row row-cols-1 row-cols-sm-2">
        {countries[level.value] &&
          flags.map((flag, index) => (
            <div className="my-small col" key={index}>
              <CSSTransition in={open} timeout={500} classNames="item">
                <img
                  src={require(`../../../../flags/${level.text}/${
                    countries[level.value][flag]
                  }.png`)}
                  alt="flag1"
                  className="flag"
                  onClick={param => guess(countries[level.value][flag])}
                />
              </CSSTransition>
            </div>
          ))}
      </TransitionGroup>
    </Fragment>
  )
}

export default Flags
