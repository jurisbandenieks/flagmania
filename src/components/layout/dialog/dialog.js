import React, { useState, useEffect, useContext, useRef } from "react"
import { Link } from "react-router-dom"
import Modal from "react-bootstrap/Modal"
import Button from "react-bootstrap/Button"
import InputGroup from "react-bootstrap/InputGroup"
import FormControl from "react-bootstrap/FormControl"

import UserContext from "../../../context/user/userContext"

const Dialog = props => {
  const [show, setShow] = useState(false)

  const myUsername = useRef("")

  const userContext = useContext(UserContext)
  const { users, addUser } = userContext

  useEffect(() => {
    setShow(props.show)
  }, [props])

  const handleClose = () => {
    setShow(false)
  }

  const onClick = () => {
    if (myUsername.current.value.length > 0) {
      addUser({ username: myUsername.current.value, score: props.points })
      myUsername.current.value = ""
    }
  }

  return (
    <Modal show={show} centered>
      <Modal.Header>
        <Modal.Title>Game Over</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="points">Your score is: {props.points}</div>
        <br />
        <InputGroup size="sm" className="mb-3">
          <InputGroup.Prepend>
            <InputGroup.Text id="inputGroup-sizing-sm">
              Your name:{" "}
            </InputGroup.Text>
          </InputGroup.Prepend>
          <FormControl
            aria-label="Small"
            aria-describedby="inputGroup-sizing-sm"
            ref={myUsername}
          />
          <InputGroup.Append>
            <Button
              aria-label="Small"
              aria-describedby="inputGroup-sizing-sm"
              variant="outline-secondary"
              onClick={onClick}
            >
              Submit
            </Button>
          </InputGroup.Append>
        </InputGroup>
      </Modal.Body>
      <ul className="list-group">
        {users.map(user => (
          <li key={user.id} className="list-group-item">
            {user.username}: {user.score} points
          </li>
        ))}
      </ul>
      <Modal.Footer>
        <Link to="/">
          <Button variant="secondary" onClick={handleClose}>
            Main Menu
          </Button>
        </Link>
      </Modal.Footer>
    </Modal>
  )
}

export default Dialog
