import React from "react"
import "./Footer.scss"

const Footer = () => {
  return (
    <div className="footer">
      <p>&copy; 2019 Juris Bandeniks All Rights Reserved</p>
    </div>
  )
}

export default Footer
