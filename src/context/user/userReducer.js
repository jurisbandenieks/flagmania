import { SET_LEVEL, INSERT_USER, GET_USERS } from "../types"

export default (state, action) => {
  switch (action.type) {
    case SET_LEVEL:
      return {
        ...state,
        level: action.payload
      }
    case GET_USERS:
      return {
        ...state,
        users: action.payload
      }
    default:
      return state
  }
}
