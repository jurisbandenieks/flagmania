import React, { useReducer } from "react"
import UserContext from "./userContext"
import UserReducer from "./userReducer"
import db from "../../firebase/init"
import { SET_LEVEL, INSERT_USER, GET_USERS } from "../types"

const UserState = props => {
  const initialState = {
    countries: [
      [
        "argentina",
        "australia",
        "austria",
        "bahamas",
        "belarus",
        "belgium",
        "bolivia",
        "brazil",
        "bulgaria",
        "canada",
        "chile",
        "china",
        "cuba",
        "czech republic",
        "denmark",
        "finland",
        "france",
        "germany",
        "greece",
        "hungary",
        "iceland",
        "india",
        "ireland",
        "italy",
        "japan",
        "malaysia",
        "mexico",
        "netherlands",
        "new zealand",
        "north korea",
        "norway",
        "panama",
        "peru",
        "philippines",
        "poland",
        "portugal",
        "romania",
        "russia",
        "saudi arabia",
        "serbia",
        "singapore",
        "slovakia",
        "slovenia",
        "south africa",
        "south korea",
        "spain",
        "sweden",
        "switzerland",
        "turkey",
        "uganda",
        "ukraine",
        "united kingdom",
        "united states of america",
        "vatican city",
        "venezuela"
      ],
      [
        "afghanistan",
        "albania",
        "algeria",
        "andorra",
        "armenia",
        "azerbaijan",
        "bahrain",
        "bangladesh",
        "barbados",
        "bosnia and herzegovina",
        "botswana",
        "cambodia",
        "cameroon",
        "central african republic",
        "colombia",
        "congo democratic republic",
        "congo republic",
        "costa rica",
        "cote d ivoire",
        "croatia",
        "cyprus",
        "dominica",
        "dominican republic",
        "ecuador",
        "egypt",
        "estonia",
        "ethiopia",
        "georgia",
        "haiti",
        "indonesia",
        "iran",
        "israel",
        "jamaica",
        "kazakhstan",
        "kenya",
        "kosovo",
        "kuwait",
        "kyrgyzstan",
        "laos",
        "latvia",
        "lebanon",
        "libya",
        "liechtenstein",
        "lithuania",
        "luxembourg",
        "macedonia",
        "malta",
        "mauritania",
        "monaco",
        "mongolia",
        "montenegro",
        "morocco",
        "myanmar",
        "namibia",
        "nepal",
        "niger",
        "nigeria",
        "oman",
        "pakistan",
        "paraguay",
        "qatar",
        "rwanda",
        "san marino",
        "sri lanka",
        "sudan",
        "syria",
        "taiwan",
        "tajikistan",
        "tanzania",
        "thailand",
        "trinidad and tobago",
        "tunisia",
        "turkmenistan",
        "united arab emirates",
        "uruguay",
        "uzbekistan",
        "vietnam",
        "zambia",
        "zimbabwe"
      ],
      [
        "angola",
        "antigua and barbuda",
        "belize",
        "benin",
        "bhutan",
        "brunei",
        "burkina faso",
        "burundi",
        "cape verde",
        "chad",
        "comoros",
        "djibouti",
        "east timor",
        "el salvador",
        "equatorial guinea",
        "eritrea",
        "fiji",
        "gabon",
        "gambia",
        "ghana",
        "grenada",
        "guatemala",
        "guinea bissau",
        "guinea",
        "guyana",
        "honduras",
        "kiribati",
        "lesotho",
        "liberia",
        "madagascar",
        "malawi",
        "maldives",
        "mali",
        "marshall islands",
        "mauritius",
        "micronesia",
        "moldova",
        "mozambique",
        "nauru",
        "nicaragua",
        "niue",
        "palau",
        "papua new guinea",
        "saint kitts and nevis",
        "saint lucia",
        "saint vincent and the grenadines",
        "samoa",
        "sao tome and principe",
        "senegal",
        "seychelles",
        "sierra leone",
        "solomon islands",
        "somalia",
        "south sudan",
        "suriname",
        "swaziland",
        "togo",
        "tonga",
        "tuvalu",
        "yemen"
      ]
    ],
    user: { username: "", score: 0 },
    level: { text: "easy", value: 0 },
    country: {},
    users: []
  }

  const [state, dispatch] = useReducer(UserReducer, initialState)

  const setLevel = props => {
    dispatch({
      type: SET_LEVEL,
      payload: props
    })
  }
  const addUser = props => {
    db.collection("users")
      .doc()
      .set({
        username: props.username,
        score: props.score
      })

    getUsers()
  }
  const getUsers = async () => {
    let response = []

    await db.collection("users").onSnapshot(snapshot => {
      snapshot.docChanges().forEach(change => {
        if (change.type == "added") {
          let doc = change.doc
          response.push({
            id: doc.id,
            username: doc.data().username,
            score: doc.data().score
          })
        }
      })
    })
    dispatch({
      type: GET_USERS,
      payload: response
    })
  }

  return (
    <UserContext.Provider
      value={{
        level: state.level,
        user: state.user,
        users: state.users,
        countries: state.countries,
        setLevel,
        addUser,
        getUsers
      }}
    >
      {props.children}
    </UserContext.Provider>
  )
}

export default UserState
