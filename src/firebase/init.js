import firebase from "firebase"
import firestore from "firebase/firestore"

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyAhuTMid2xsC20q8_zqYCrdxQfPS7ran5A",
  authDomain: "flagmania-9c872.firebaseapp.com",
  databaseURL: "https://flagmania-9c872.firebaseio.com",
  projectId: "flagmania-9c872",
  storageBucket: "flagmania-9c872.appspot.com",
  messagingSenderId: "486111151151",
  appId: "1:486111151151:web:50f8f43c0a1c9df8c8f2da"
}
// Initialize Firebase
const firebaseApp = firebase.initializeApp(firebaseConfig)

//export firestore DB

export default firebaseApp.firestore()
